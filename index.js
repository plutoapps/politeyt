const express = require('express')
const PORT = process.env.PORT || 5000
const cors = require('cors')
const fetch = require('node-fetch');
const stream = require('stream');

express()
  .use(cors())
  .use(express.static('public'))
  .get('/api/video', video)
  .get('/api/video2', video2)
  .get('/api/share.mp4', shareLink)
  .get('/play.mp4', play)
  .get('/download.mp4', download)
  .listen(PORT, () => console.log(`Listening on http://localhost:${PORT}`))

function youtubeFetch(videoId) {
  const payload = {
    videoId,
    context: {
      client: {
        hl: "en",
        gl: "US",
        clientName: "ANDROID_EMBEDDED_PLAYER",
        clientVersion: "16.02",
      },
    },
  };
  return fetch(
    "https://youtubei.googleapis.com/youtubei/v1/player?key=AIzaSyAO_FJ2SlqU8Q4STEHLGCilw_Y9_11qcW8",
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(payload),
    }
  );
}

function filterFormats(format) {
  return format.mimeType.indexOf("video/mp4") === 0;
}
function sortFormats(a, b) {
  if (a.bitrate > b.bitrate) {
    return -1;
  }
  if (a.bitrate < b.bitrate) {
    return 1;
  }
  return 0;
}

async function youtubeFromvideoId(videoTargets) {
  const resultPromise = videoTargets.map(async (target) => {
    const response = await youtubeFetch(target);
    const data = await response.json();
    return {
      source: data.streamingData.formats
        ?.filter(filterFormats)
        ?.sort(sortFormats),
      detail: data.videoDetails,
      thumbnail: data.videoDetails?.thumbnail?.thumbnails?.pop()?.url,
      link: 'https://www.youtube.com/watch?v=' + data.videoDetails.videoId
    };
  });
  return Promise.all(resultPromise);
}

async function video(req, res) {
  const videoTargets = req.query.id.split(',');
  res.send(await youtubeFromvideoId(videoTargets));
}

async function shareLinkStream(filename, url, res) {

  const resonse = await fetch(url);
  const buff = await resonse.buffer();

  var readStream = new stream.PassThrough();
  readStream.end(buff);

  res.set('Content-disposition', 'attachment; filename=' + filename + '.mp4');
  res.set('Content-Type', 'video/mp4');

  readStream.pipe(res);
}

function shareLink(req, res) {
  const source = Buffer.from(req.query.source, 'base64').toString();
  const sourceParts = source.split(',');
  shareLinkStream(sourceParts[0], sourceParts[1], res);
}

async function play(req, res) {
  const data = (await youtubeFromvideoId([req.query.id]))[0];
  let shareName = data.detail.title.replace(/[^A-Za-z0-9 ]+/g, '');
  let url = data.source[0].url;
  for (let i = 0; i < data.source.length; i++) {
    if (data.source[i].qualityLabel == req.query.qualityLabel) {
      url = data.source[i].url;
      break;
    }
  }
  const resonse = await fetch(url);
  const buff = await resonse.buffer();
  res.end(buff);
}

async function download(req, res) {
  const data = (await youtubeFromvideoId([req.query.id]))[0];
  let shareName = data.detail.title.replace(/[^A-Za-z0-9 ]+/g, '');
  let url = data.source[0].url;
  for (let i = 0; i < data.source.length; i++) {
    if (data.source[i].qualityLabel == req.query.qualityLabel) {
      url = data.source[i].url;
      break;
    }
  }
  shareLinkStream(shareName, url, res)
}


const Innertube = require('youtubei.js');
let youtube;
async function initInnertuble() {
  youtube = await new Innertube();
};
initInnertuble();

async function video2(req, res) {
  const videoTargets = req.query.id.split(',');
  const promises = [];
  for (let i = 0; i < videoTargets.length; i++) {
    promises.push(youtube.getStreamingData(videoTargets[i], {
      format: 'mp4',
      quality: '720p',
      type: 'videoandaudio'
    }));
  }
  const result = await Promise.all(promises);
  res.send(result);
}
